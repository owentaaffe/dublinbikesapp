// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','dublinbike.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
  
  .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
        controller: 'loginCtrl'
      }
    }
  })
  
  .state('app.signup', {
      url: '/signup',
      views: {
        'menuContent': {
          templateUrl: 'templates/signup.html',
        //  controller: 'signupCtrl'
        }
      }
    })

  .state('app.map', {
    url: '/map',
    views: {
      'menuContent': {
        templateUrl: 'templates/map.html',
        controller: 'MapCtrl'
      }
    }
  })

  .state('app.feedback', {
      url: '/feedback',
      views: {
        'menuContent': {
          templateUrl: 'templates/feedback.html'
        }
      }
    })
    .state('app.home', {
      url: '/home',
      views: {
        'menuContent': {
          templateUrl: 'templates/home.html'
        }
      }
    })

  .state('app.help', {
    url: '/help',
    views: {
      'menuContent': {
        templateUrl: 'templates/help.html',
       // controller: 'HelpCtrl'
      }
    }
  })
  
  .state('app.list', {
      url: '/list',
      views: {
          'menuContent': {
              templateUrl: 'templates/list.html',
            //  controller: 'ListCtrl'
          }
      }
  })
  
  .state('app.edit', {
      url: '/edit/:noteId',
      views: {
       'menuContent': {
            templateUrl: 'templates/edit.html',
           // controller: 'EditCtrl'
       }
     }
  })
  
  .state('app.bikes', {
      url: '/bikes',
      views: {
        'menuContent': {
          templateUrl: 'templates/bikes.html',
          controller: 'BikesCtrl'
        }
      }
    })
  
 .state('app.bikeDetail', {
      url: '/bikes/:bikeId',
       views: {
        'menuContent': {
          templateUrl: 'templates/bike-detail.html',
          controller: 'BikeDetailCtrl'
        }
      }
    }) 
  
  
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
