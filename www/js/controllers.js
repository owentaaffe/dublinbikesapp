angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('MapCtrl',function ($scope, DublinBikeService) {
    
    $scope.$on('mapInitialized', function (event, map){
        
       $scope.map = map;
       
   })
   
    $scope.dbs = DublinBikeService;
    
    $scope.dbs.load();
    //$scope.lat = 53.456;
   // $scope.lon = -6.456;
   //Import to init for map events
   
   $scope.showStationDetail = function (event, bikeStation) {
       
       $scope.dbs.currentBikeStation = bikeStation;
       
       $scope.map.showInfoWindow.apply(this,[event,'BikeStationInfo']);
          
       console.log("The name of the station is"  + bikeStation.name);
       
   }
    
})

.controller('BikesCtrl', function($scope, $stateParams, $http) {
    
    var key = "11899ba30984009def869e492dd0ef22c9797f0e";
    
        $http.get("https://api.jcdecaux.com/vls/v1/stations?contract=dublin&apiKey=" + key).success(function (data) {
    
    $scope.bikes = data;
    
  });    
})

.controller('BikeDetailCtrl', function($scope, $stateParams, $http) {
    
    var key = "11899ba30984009def869e492dd0ef22c9797f0e";
    
       $http.get("https://api.jcdecaux.com/vls/v1/stations?contract=dublin&apiKey=" + key).success(function (data) {
    
    var locationNumber = $stateParams.bikeId;
    
    for(var i in data) {       
        if (data[i].number == locationNumber) {        
            $scope.bike = data[i];         
        }        
    }    
  });        
})

.controller('HomeCtrl', function($scope, $ionicPopup, $timeout, DublinBikeService) {
    $scope.goNext = function (hash) {
        $location.path(hash);
    };
});
